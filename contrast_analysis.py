#!/usr/bin/env python
# -*- coding: utf-8 -*-

from cortextlib.legacy import containerize

import os, sys
print "os.getcwd()",os.getcwd()
reload(sys) 
sys.setdefaultencoding("utf-8")
from sqlite3 import *
import pandas as pd
from librarypy.path import *
from librarypy import fonctions
import sqlite3
import shutil
import scattertext as st
import spacy
import logging, pprint
try:
	print 'user_parameters',user_parameters
except:
	user_parameters=''
	
parameters_user=fonctions.load_parameters(user_parameters)

#####PARAMETERS
data_source = parameters_user.get('corpus_file','')
result_path=parameters_user.get('result_path','')
result_path0=result_path[:]
text_table =parameters_user.get('text_table','')
contrastive_table=parameters_user.get('contrastive_table','')
target_category=parameters_user.get('target_category','')
contrasted=parameters_user.get('contrasted',False)
contrasted_by=parameters_user.get('contrasted_by','')
addmeta=parameters_user.get('addmeta',False)
meta=parameters_user.get('meta','')
categorical=parameters_user.get('categorical','text')
mimimal_frequency=int(parameters_user.get('mimimal_frequency',5))
max_terms=int(parameters_user.get('max_terms',1000))
simple_token=parameters_user.get('simple_token',True)
#if categorical=='text':
nlp=spacy.load(parameters_user.get('language','en'))


fonctions.check_bddname(data_source)
filename_v = fonctions.get_data(data_source,'ISIpubdate',limit_id=1)
try:
	filename=filename_v.values()[0][0]['file']
except:
	filename='f'

#################################
###logging user parameters#######
#################################
logging.basicConfig(filename=os.path.join(result_path,'.user.log'), filemode='w', level=logging.DEBUG,format='%(asctime)s %(levelname)s : %(message)s', datefmt="%Y-%m-%d %H:%M:%S")
logging.info('Script Contrast Analysis Started')
yamlfile = 'contrast_analysis.yaml'
if 'cointet' in os.getcwd():
	yamlfile = '/Users/jpcointet/Desktop/cortext-methods/contrast_analysis/'+yamlfile
parameterslog=fonctions.getuserparameters(yamlfile,parameters_user)
logging.info(parameterslog)
fonctions.progress(result_path0,2)



##################################
### end logging user parameters###
##################################
#####FUNC
def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d


#####CODE
print 'trying to open', data_source
conn,curs = fonctions.create_bdd(os.path.join(result_path,data_source))


fonctions.progress(result_path0,10)
logging.info('Loading data')

curs.execute("SELECT * FROM sqlite_master WHERE type='table'")
results = curs.fetchall()
existing_tables=map(lambda x:x[1],results)
print 'len(authorized_tables)',len(existing_tables)
print 'text_table',text_table
try:
	text_data_iter=fonctions.get_results(text_table,curs, where=None,rank=True,unicity=False).iteritems()
except:
	logging.debug('Impossible to retrieve data from the text field your chose, please double check your corpus')
	djsklq
try:
	category_data_dict=fonctions.get_results(contrastive_table,curs, where=None,rank=True,unicity=False).iteritems()
except:
	logging.debug('Impossible to retrieve data from the categorical field your chose, please double check your corpus')
	djsklq
	
meta_data_iter_dict={}
if addmeta:
	for met in meta:
		meta_data_iter_dict[met]=fonctions.get_results(met,curs, where=None,rank=True,unicity=False).iteritems()
#print 'text_data_dict',text_data_dict# for id,vald in text_data_dict:
text_data_dict={}
for id,val in text_data_iter:
	if categorical=='text':
		text_data_dict[id]=' '.join(val.keys())
	else:
		text_data_dict[id]=' '.join(map(lambda x: x.replace(' ','_'),val.keys()))
if addmeta:
	meta_data_dict={}
	for met in meta:
		meta_data_dic={}
		for id,val in meta_data_iter_dict[met]:
			meta_data_dic[id]=val.keys()[0]
		meta_data_dict[met]=meta_data_dic
# 	print id,vald
dico_cat2id={}
dico_id2cat={}
for id,vald in category_data_dict:
	#for cle in vald:
		#print "vald",vald
		if target_category in vald.keys():
			dico_cat2id.setdefault(target_category,[]).append(id)
			dico_id2cat.setdefault(id,[]).append(target_category)
		else:
			if contrasted:
				if contrasted_by in vald.keys():
					dico_cat2id.setdefault('other',[]).append(id)
					dico_id2cat.setdefault(id,[]).append('other')
			else:
				dico_cat2id.setdefault('other',[]).append(id)
				dico_id2cat.setdefault(id,[]).append('other')

if len(dico_cat2id.get(target_category,[])) * len(dico_cat2id.get('other',[]))==0:
	logging.debug("One of the target category does not exist in your dataset")
	jdlk
id_list=[]
text_list=[]
meta_list_dict={}
for id in dico_id2cat:
	#print 'id',id
	for cats in dico_id2cat[id]:
		id_list.append(cats)
		try:
			#print 'text_data_dict[id]',text_data_dict[id]
			text_list.append(text_data_dict[id])
		except:
			text_list.append('')
		if addmeta:
			for met in meta:
				meta_list_dict.setdefault(met,[]).append(meta_data_dict.get(met,{}).get(id,''))
data = {'cat': id_list, 'text': text_list}
if addmeta:
	for met in meta:
		data[met]=meta_list_dict[met]
	
categorized_text_data=pd.DataFrame.from_dict(data)	
print categorized_text_data.head()
print categorized_text_data.columns
conn.close()


def hashtag_pipe(doc):
    merged_hashtag = False
    while True:
        for token_index,token in enumerate(doc):
            if token.text == '#':
                if token.head is not None:
                    start_index = token.idx
                    end_index = start_index + len(token.head.text) + 1
                    if doc.merge(start_index, end_index) is not None:
                        merged_hashtag = True
                        break
        if not merged_hashtag:
            break
        merged_hashtag = False
    return doc

	
print ('building the corpus')
print 'categorical',categorical
print 'simple_token',simple_token
if categorical=='text':
	if simple_token=='naive':
		corpus = st.CorpusFromPandas(categorized_text_data, category_col='cat', text_col='text',nlp=st.whitespace_nlp_with_sentences).build()
	elif simple_token=='Twitter':
		print 'here we are processing tweets'
		nlp.add_pipe(hashtag_pipe)
		corpus = st.CorpusFromPandas(categorized_text_data, category_col='cat', text_col='text',nlp= nlp).build()
	else:
		corpus = st.CorpusFromPandas(categorized_text_data, category_col='cat', text_col='text',nlp=nlp).build()
else:
	#corpus = st.CorpusFromPandas(categorized_text_data, category_col='cat', text_col='text',nlp=st.whitespace_nlp_with_sentences).build()
	corpus = st.CorpusFromPandas(categorized_text_data, category_col='cat', text_col='text',nlp=nlp).build()
#if parameters_user.get('only_mono',True):
	corpus=corpus.get_unigram_corpus()
print ('corpus built')
#corpus2=st.CorpusFromPandas(categorized_text_data, category_col='cat', text_col='text',nlp=st.whitespace_nlp_with_sentences).build().get_unigram_corpus().compact(st.ClassPercentageCompactor(term_count=2,term_ranker=st.OncePerDocFrequencyRanker))
distinct50=list(corpus.get_scaled_f_scores_vs_background().index[:50])
print "here are the terms of the corpus which differ the most with standard language"
print(distinct50)
logging.info('Most distinct words in your corpus (compared to standard language)' +  pprint.pformat(distinct50))

print "corpus.get_scaled_f_scores_vs_background().head()",corpus.get_scaled_f_scores_vs_background().head()
contrast_background=corpus.get_scaled_f_scores_vs_background()



term_freq_df = corpus.get_term_freq_df()
term_freq_df['Democratic Score'] = corpus.get_scaled_f_scores(target_category)
most_target = list(term_freq_df.sort_values(by='Democratic Score', ascending=False).index[:50])
if contrasted:
	term_freq_df['Counter Democratic Score'] = corpus.get_scaled_f_scores('other')
	most_counter_target = list(term_freq_df.sort_values(by='Counter Democratic Score', ascending=False).index[:50])
else:
	most_counter_target = list(term_freq_df.sort_values(by='Democratic Score', ascending=True).index[:50])

print 'term_freq_df',term_freq_df.head()
print term_freq_df.columns
try:
	feed=open(os.path.join(result_path,'frequency_statistics_'+target_category.replace(' ','_')+'_'+contrasted_by.replace(' ','_')+'.csv'),'w')
except:
	feed=open(os.path.join(result_path,'frequency_statistics_'+target_category.replace(' ','_')+'.csv'),'w')
feed.write('term\t\frequency in class '+ target_category +'\t'+'frequency in the other class'+'\t'+'total frequency'+'\t'+'F-scaled score (class)'+'\t'+'F-scaled score (other)'+'\t'+'background characterisness\n')
for index, row in term_freq_df.iterrows():
	#print index
	try:
		feed.write('\t'.join(map (lambda x: str(x), [index,int(row[target_category+ ' freq']),int(row[ 'other freq']) ,int(row[target_category+ ' freq'])+int(row[ 'other freq']) , row['Democratic Score'],row['Counter Democratic Score'],contrast_background.loc[index]['Scaled f-score']]))+'\n')
	except:
		try:
			feed.write('\t'.join(map (lambda x: str(x), [index,int(row[target_category+ ' freq']),int(row[ 'other freq']) ,int(row[target_category+ ' freq'])+int(row[ 'other freq']) , row['Democratic Score'],'',contrast_background.loc[index]['Scaled f-score']]))+'\n')
			
		except:
			feed.write('\t'.join(map (lambda x: str(x), [index,int(row[target_category+ ' freq']),int(row[ 'other freq']) ,int(row[target_category+ ' freq'])+int(row[ 'other freq']) , row['Democratic Score'],'','']))+'\n')
feed.close()
print "here are the most associated terms of the corpus which the category " + target_category
print(most_target)
logging.info('Words most specifically associated with the category ' +target_category+  pprint.pformat(most_target))

if contrasted:
	print "here are the most associated terms of the corpus which the category " + contrasted_by
	print(most_counter_target)
	logging.info('Words most specifically associated with the category ' +contrasted_by+  pprint.pformat(most_counter_target))
else:
	print "here are the least associated terms of the corpus which the category " + target_category
	print(most_counter_target)
	logging.info('Words least specifically associated with the category ' +target_category+  pprint.pformat(most_counter_target))
	
if contrasted:
	not_category_name = contrasted_by
else:
	not_category_name = 'not '+ target_category
if addmeta:
	html = st.produce_scattertext_explorer(corpus,category=target_category,category_name=target_category,not_category_name=not_category_name,width_in_pixels=1000,minimum_term_frequency=max(0,mimimal_frequency),save_svg_button=True,metadata=corpus.get_df()[meta],max_terms=max_terms)
	#html2 = st.produce_characteristic_explorer(corpus.get_unigram_corpus().compact(st.ClassPercentageCompactor(term_count=20,term_ranker=st.OncePerDocFrequencyRanker)),category=target_category,category_name=target_category,not_category_name=not_category_name,metadata=corpus.get_df()[meta])
else:
	html = st.produce_scattertext_explorer(corpus,category=target_category,category_name=target_category,not_category_name=not_category_name,width_in_pixels=1000,minimum_term_frequency=max(0,mimimal_frequency),save_svg_button=True,max_terms=max_terms)
	#html2 = st.produce_characteristic_explorer(corpus,category=target_category,category_name=target_category,not_category_name=not_category_name)
	#html2 = st.produce_characteristic_explorer(corpus2,category=target_category,category_name=target_category,not_category_name=not_category_name,width_in_pixels=1000)

# add edit labels button

edit_tag_content = """
	<div style="margin: 0.5rem;">
  <a id="svg-editor-link" style="
        color:#FFFFFF;
        text-align: center;
        padding-left: 0.5rem;
        padding-right: 0.5rem;
        padding-top: 0.25rem;
        background:#48bb78;
        border: 1px solid #2f855a;
        border-radius: 0.25rem;
        cursor: pointer;
        /*! margin: 0.5rem; */
        white-space: nowrap;
      ">
    Edit labels
  </a>
</div>
"""

script_tag_content = '<script src="https://labeleditor.cortext.net/svg_postmessage.js"></script>'

html = html + edit_tag_content + script_tag_content

open(os.path.join(result_path,"Contrast_Visualization.html"), 'wb').write(html.encode('utf-8'))
#open(os.path.join(result_path,"Contrast_Visualization2.html"), 'wb').write(html2.encode('utf-8'))



from librarypy import descriptor
descriptor.generate(data_source)
logging.info('Contrast Analysis script ended successfully')
fonctions.progress(result_path0,100)
